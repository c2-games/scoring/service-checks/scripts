# Scoring Service-check Scripts

Contains scripts to score services against checks written in Python. Scripts exit with an exit
code of 0 and a status in a JSON blob that corresponds to the following enum:

| Status Code | Explanation/Notes |
|--------|--------------------------------------------|
| success | Check was completely successful |
| failure | Check was completely unsuccessful |
| partial | Partial success |
| timeout | Some kind of timeout occurred |
| unknown | An unknown failure occurred |
| error | A code error in the script occurred. Whoops. |

The results library can be found [here](https://gitlab.com/c2-games/scoring/service-checks/cyboard-check-module)

Clicking on a package version from the [registry](https://gitlab.com/c2-games/scoring/service-checks/cyboard-check-module/-/packages/)
will contain more information about how to install the PyPI package. Or you can download the wheel and install as well.

## Check Scripts and Arguments

| Service          | Script & Usage                             | Notes                                                                                                                                                                             |
|------------------|--------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Ping             | `check_icmp.py [-t timeout]`               |
| Web 80           | `check_http.py [-t timeout]`               |                                                                                                                                                                                   |
| Web Content      | `check_web_content.py`                     |                                                                                                                                                                                   |
| Web SSL          | `check_web_content.py --ssl`               | Also checks that content is working correctly                                                                                                                                     |
| SSH Login        | `check_ssh.py [-t timeout]`                |                                                                                                                                                                                   |
| FTP Login        | `check_ftp.py --check-login`               |                                                                                                                                                                                   |
| FTP Write        | `check_ftp.py --check-write`               |                                                                                                                                                                                   |
| FTP Content      | `check_ftp.py --check-size [--check-hash]` | The `--check-hash` option will download several files, hash them locally, and verify the expected hash. This is slightly more expensive than the size integrity check.            |
| MySQL Read/Write | `check_mysql.py`                           | **SCORED INTERNALLY**.                                                                                                                                                            |
| DNS FWD INT      | `check_dns.py --forward`                   | **SCORED INTERNALLY**.  |
| DNS FWD EXT      | `check_dns.py --forward`                   |                         |
| DNS REV INT      | `check_dns.py --reverse`                   | **SCORED INTERNALLY**.  |
| DNS REV EXT      | `check_dns.py --reverse`                   |                         |

## Developing

All scripts expect two arguments, which normally are sent via the
[Check Daemon](https://gitlab.com/c2-games/scoring/service-checks/check-daemon). Scripts developed locally must provide
these arguments manually:

- `-C`/`--check-name`: Name of the check being executed, as it corresponds to the check name in `event.yml`
- `--vars`: Filepath to JSON file with all variables. Typically, this generated using the check daemon.

### Generating Variables File

Previously used variables are stored in the [Support repo](https://gitlab.com/c2-games/support), usable vars.json files
can be generated from this repo using the Check Daemon project.

1. Clone the Check-daemon and get it built locally (follow instructions in check-daemon readme)
2. From support repo, run the following command. Substitute Team and Check values with the relevant values you're checking.

    ```sh
    check-daemon vars -i event:event_testrun.yml -i team:team_team01.json -i check:check_service_www_content.yml > www_content.json
    ```

3. After copying www_content.json to the scripts repo, a check script can now be run using this command:

    ```sh
    python check_www_content.py --vars www_content.json --check-name external_www_content
    ```

> **Note**: Check name should be the same as the defined check in event.yml. If testing against non-production IP addresses,
> either update event.yml to use the dev IP, or include a deployment file with `-i deploy:deploy.yml`
>
> ```yaml
> # deploy.yml
> host: 192.168.1.1
> ```
>

### Service check script required output format

Each service check script should exit with a success code.
The script should return a json object to stdout in the following format:

```json
{
    "result": "success",
    "extendedData": {
        "participant": {
            "feedback": "Customizable feedback. Logic built into service check script to give meaninful info",
            "detailed_results": []  // Optional Detailed results.
        },
        "staff": {
            "feedback": "Optional feedback to staff only",
            "detailed_results": []  // Optional Detailed staff-only results.
        }
    }
}
```

Example

```json
{
  "result": "failure",
  "extendedData": {
    "participant": {
      "feedback": "19 files are missing on host 172.18.14.1 as brock",
      "detailed_results": [
        {
          "missing_files": [
            "Celebi.bin",
            // ...
            "Shaymin.bin"
          ],
          "incorrect_files": []
        }
      ]
    },
    "staff": {
      "feedback": "All files are missing, maybe the drive with the files isn't mounted?",
      "detailed_results": [
        {
          "host": "172.18.14.1",
          "username": "brock",
          "password": "P@$$w0rd"
        }
      ]
    }
  }
}
```
