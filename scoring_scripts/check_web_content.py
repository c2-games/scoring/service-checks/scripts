#!/usr/bin/env python3

import argparse
import random
import socket
import tempfile
from ssl import SSLError
from typing import Optional, List
from urllib.parse import urlparse, urlunparse

import urllib3
import urllib3.exceptions
import activesoup
import dns.resolver
import dns.exception
import requests
import requests.adapters
from pydantic import BaseModel, validator, Field
from faker import Faker

from cyboard_check import C2GamesCheck, ResultCode


def noop(*_, **__):
    """absorb all args."""


class HostnameResolutionError(requests.exceptions.RequestException):
    """Exception raised when the hostname of a system cannot be resolved"""


class NameserversAdapter(requests.adapters.HTTPAdapter):
    def __init__(self, nameservers, server_hostname, print_=noop):
        self.nameservers = nameservers
        self.server_hostname = server_hostname
        self.print = print_
        super().__init__()

    def _resolve(self, host, nameservers, record_type) -> Optional[str]:
        resolver = dns.resolver.Resolver()
        resolver.nameservers = nameservers
        self.print('resolving host', host)
        try:
            # return the first result if there is one
            ans = resolver.resolve(host, record_type)
            self.print('answer', ans[0])
            return str(ans[0])
        except (dns.resolver.NXDOMAIN, dns.exception.Timeout, dns.resolver.NoNameservers):
            return None

    def get_connection(self, url, proxies=None):
        """Implement the get_connection method"""
        parts = urlparse(url)
        ip = self._resolve(parts.hostname, self.nameservers, 'A')
        if not ip:
            raise HostnameResolutionError(f"could not resolve {parts.hostname}")
        new_netloc = ip
        if parts.port:
            new_netloc = f'{ip}:{parts.port}'
        # Make a new dict replacing hostname with ip
        updated = parts._replace(netloc=new_netloc)

        # overwrite the server hostname, since we're setting the connection IP manually
        # https://stackoverflow.com/questions/22609385/python-requests-library-define-specific-dns
        connection_pool_kwargs = self.poolmanager.connection_pool_kw
        if parts.scheme == 'https':
            connection_pool_kwargs['server_hostname'] = self.server_hostname
            connection_pool_kwargs['assert_hostname'] = self.server_hostname
        else:
            # theses headers from a previous request may have been left
            connection_pool_kwargs.pop('server_hostname', None)
            connection_pool_kwargs.pop('assert_hostname', None)

        return super().get_connection(urlunparse(updated), proxies=proxies)


class CustomDriver(activesoup.driver.Driver):
    """Custom Driver"""
    def __init__(self, ca_bundle=None, nameservers=None, print_=noop):
        super().__init__()
        self.session = requests.Session()
        self.session.headers.update({"User-agent": Faker().user_agent()})
        self.session.verify = ca_bundle or False
        self._nameservers = nameservers or []
        self.print = print_

    @property
    def nameservers(self) -> List[str]:
        """List of nameservers to use for hostname resolution"""
        return self._nameservers

    @nameservers.setter
    def nameservers(self, nameservers: List[str]):
        """Set list of nameservers to use for hostname resolution"""
        self._nameservers = nameservers

    def _do(self, request: requests.Request):
        hostname = urlparse(request.url).hostname
        # only enter this block if hostname is known
        # form_data.post() requests will use a partial url ('admin.php'),
        # instead of a full url with a hostname.
        # We can simply leave the adapters from previous requests mounted instead
        if self.nameservers and hostname:
            # register custom adapter as default handler for http and https, as well as for this specific url
            self.session.mount(request.url, NameserversAdapter(self.nameservers, hostname, print_=self.print))
            self.session.mount('http://', NameserversAdapter(self.nameservers, hostname, print_=self.print))
            self.session.mount('https://', NameserversAdapter(self.nameservers, hostname, print_=self.print))
            # Adding the host header helps identify the target when the ip is
            # inserted by the resolver. Don't override a selected host though.
            if "Host" not in request.headers:
                request.headers["Host"] = hostname
        return super()._do(request)


def extract_ssl_error(exc: requests.exceptions.ConnectionError) -> Optional[str]:
    """
    Extracts a requests.exceptions.SSLError from a requests ConnectionError
    """
    ssl_errors = (SSLError, requests.exceptions.SSLError, urllib3.exceptions.SSLError)
    if exc and getattr(exc, 'args', None):
        for arg in exc.args:
            if getattr(arg, 'reason', None) and isinstance(arg.reason, ssl_errors):
                return str(arg.reason)
    return None


class C2WebConfig(BaseModel):
    host: str
    port: int
    ssl: bool
    username: str
    path: str
    password: str
    post_title: str
    web_theme_things: List[str]
    nameservers: List[str]

    @validator('path')
    def path_starts_with_forward_slash(cls, v: str):  # pylint: disable=no-self-argument
        """Ensure Path starts with a forward slash"""
        if not v.startswith('/'):
            v = f"/{v}"
        return v

    @property
    def web_schema(self):
        """Return schema used for HTTP request"""
        return 'https' if self.ssl else 'http'

    @property
    def url(self):
        """Return URL to access website"""
        return f"{self.web_schema}://{self.host}:{self.port}{self.path}"

    @property
    def http_auth_url(self):
        """Return URL to access website"""
        return f"{self.web_schema}://{self.username}:{self.password}@{self.host}:{self.port}{self.path}"

    def get_post_content(self):
        """Get random content from the supplied list for the web post, and escape it for safe use."""
        return random.choice(self.web_theme_things).replace("'", "\\'")


class WebContentCheck(C2GamesCheck):
    driver: CustomDriver = Field(default_factory=CustomDriver)

    def __init__(self, *args, **kwargs):
        parser = argparse.ArgumentParser()
        parser.add_argument('-p', '--port', metavar='PORT', required=False, help='port to target',
                            type=int, default=None)
        parser.add_argument('-P', '--path', metavar='ADDRESS', required=False,
                            help='page to retrieve', default='/')
        parser.add_argument('-S', '--ssl', action='store_true', help='Use SSL')
        super().__init__(parser=parser, *args, **kwargs)

        nameservers = []
        if hasattr(self.variables.check, 'nameservers'):
            nameservers = [self.variables.jinja_render_vars(n) for n in self.variables.check.nameservers]
            self.print('using custom nameservers', nameservers)

        self.config: C2WebConfig = C2WebConfig(
            host=self.variables.jinja_render_vars(getattr(self.variables.check, "hostname", self.host)),
            ssl=self.args.ssl,
            path=self.args.path,
            port=self.args.port or 443 if self.args.ssl else 80,
            username=self.variables.check.web_username,
            password=self.variables.check.web_password,
            post_title=self.variables.check.post_title,
            web_theme_things=self.variables.check.web_theme_things,
            nameservers=nameservers
        )

        # pass in debug print func and nameservers
        self.driver.print = self.print
        self.driver.nameservers = nameservers

    def setup_ssl(self, ssl_ca_bundle_file):
        """Create a new driver using the configured SSL CA Bundle"""
        # Write a temp file for cert, ugly, maybe do function patching later
        ssl_ca_bundle_file.write(self.variables.check.root_ca.encode())
        ssl_ca_bundle_file.flush()
        self.driver = CustomDriver(ca_bundle=ssl_ca_bundle_file.name)
        self.driver.nameservers = self.config.nameservers

    def execute_web_check(self) -> None:
        """Check that the site is functioning as expected - Ex, create a post and read it again."""
        raise NotImplementedError()

    def execute(self):
        """Execute the Check."""
        self.print(f'targeting {self.config.url}')
        # pylint: disable=consider-using-with
        ssl_ca_bundle_file = tempfile.NamedTemporaryFile()
        self.print('created tmp file', ssl_ca_bundle_file.name)
        if self.config.ssl:
            self.setup_ssl(ssl_ca_bundle_file)

        try:
            self.print('executing attempt_content_check()')
            self.execute_web_check()
            self.result.success(feedback=f"Web content working{' and secure' if self.args.ssl else ''}!")
        except HostnameResolutionError as exc:
            self.result.exit(
                feedback=str(exc),
                staff_details={'raw': str(exc)},
                status=ResultCode.FAIL
            )
        except requests.exceptions.ConnectionError as exc:
            feedback = extract_ssl_error(exc)  # try to extract an SSL error
            if not feedback:
                # If we didn't find an SSL error, default to "Failed to connect to host"
                feedback = "Failed to connect to host"

            self.result.exit(
                feedback=feedback, staff_details={'raw': str(exc)},
                status=self.attempt_partial()
            )
        except AssertionError as exc:
            self.result.warn(
                feedback=str(exc), staff_details={'raw': str(exc)}
            )
        finally:
            if ssl_ca_bundle_file:
                self.print('removing tmp file', ssl_ca_bundle_file.name)
                ssl_ca_bundle_file.close()

    def attempt_partial(self):
        """Attempt connection to web port for partial check."""
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            # create an INET, STREAMing socket
            # now connect to the web server on port 80
            sock.connect((self.config.host, self.config.port))
            sock.send(b'some random data')
            # data = s.recv(4096)
            self.result.add_detail(f"Raw Connection to port {self.config.port} succeeded")
            return ResultCode.WARN
        except Exception as e:
            self.result.add_detail(f"Raw Connection to port {self.config.port} failed")
            self.result.add_staff_detail({'raw': str(e), 'error': f'failed raw connection on port {self.config.port}'})
            return ResultCode.FAIL
        finally:
            sock.close()


if __name__ == "__main__":
    WebContentCheck().execute()
