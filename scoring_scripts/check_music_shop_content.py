#!/usr/bin/env python3
from random import randint

from check_web_content import WebContentCheck  # pylint: disable=import-error


class MusicShopCheck(WebContentCheck):
    @classmethod
    def generate_cart(cls) -> str:
        """Generate valid cart cookie string"""
        price_lookup_table = {
            1: "120.00",
            2: "70.00",
            3: "50.00",
            4: "25.00",
            5: "100.00",
            6: "10.00",
            7: "8.00",
            8: "15.00",
            9: "20.00",
            10: "45.00",
            11: "30.00",
            12: "12.00",
            13: "22.00",
            14: "5.00",
            15: "40.00",
        }
        item_id = randint(1, 15)
        return (
            f'[{{"id":{item_id},"price":{price_lookup_table[item_id]},"quantity":1}}]'
        )

    def checking_failed_orders(self) -> None:
        """Attempt an invalid order to validate site fails"""
        # Generating known bad cart cookie
        try:
            self.driver.session.cookies.set(
                "cart",
                f'[{{"id": "{randint(9000,99999)}","price": {randint(1,100)}, "quantity": {randint(1,20)}}}]',
                domain=self.config.host,
            )
            order_result = self.driver.session.get(
                f"{self.config.url}/purchase", allow_redirects=True, timeout=1
            )
        except BaseException as e:
            self.result.warn(
                feedback=f"{self.config.username} was not able to request their shopping cart",
                staff_details={"raw": str(e), "error_type": str(type(e))},
            )
        # Website should throw a 500 error
        if order_result.status_code != 500:
            self.result.warn(
                feedback="{self.config.username} encountered an error while checking out",
                staff_feedback="Invalid order successfully processed",
            )

        # Deleting bad cart cookie
        self.driver.session.cookies.clear(
            name="cart", domain=self.config.host, path="/"
        )

    def checking_orders(self) -> None:
        """Attempt to order an item"""
        try:
            self.driver.session.cookies.set(
                "cart", self.generate_cart(), domain=self.config.host
            )
            order_result = self.driver.session.get(
                f"{self.config.url}/purchase", allow_redirects=True, timeout=1
            )
        except BaseException as e:
            self.result.warn(
                feedback=f"{self.config.username} was not able to request their shopping cart",
                staff_details={"raw": str(e), "error_type": str(type(e))},
            )

        if order_result.status_code != 200:
            self.result.warn(
                feedback=f"{self.config.username} encountered an error while checking out",
                staff_details={
                    "status": f"return error::{order_result.status_code}",
                    "cookies": self.driver.session.cookies.values(),
                },
            )
        # This final error should be unlikely; this is the best indicator the order actually went through
        if self.driver.session.cookies["cart"] != "[]":
            self.result.warn(
                feedback="{self.config.username} encountered an error while checking out",
                staff_details="error clearing cookie on website side",
            )

    def music_shop_login(self) -> None:
        """Attempt Music Shop Login"""
        login_info = {
            "username": self.config.username,
            "password": self.config.password,
        }
        try:
            self.driver.session.post(
                f"{self.config.url}/login.html",
                data=login_info,
                allow_redirects=True,
                timeout=1,
            )
        except BaseException as e:
            self.result.fail(
                feedback=f"{self.config.username} was not able to request the login page",
                staff_details={"raw": str(e), "error_type": str(type(e))},
            )
        response_domains = self.driver.session.cookies.list_domains()
        # this handles if we're redirected to teamX.ncaecybergames.org instead of the IP address
        if response_domains:
            self.config.host = response_domains[0]

        if "session_id" not in self.driver.session.cookies:
            self.result.fail(
                feedback=f"{self.config.username} was unable to login",
                details={
                    "username": self.config.username,
                    "password": self.config.password,
                    "base_url": self.config.url,
                    "using_ssl": self.config.ssl,
                },
                staff_feedback="session_id cookie not found in session",
                staff_details={
                    "user": self.config.username,
                    "pass": self.config.password,
                    "raw": "session token was not generated",
                    "cookies": f"{self.driver.session.cookies.values()}",
                },
            )

    def content_check(self) -> None:
        """Checking for Music Shop Copyright message"""
        try:
            result = self.driver.get(f"{self.config.url}/index.html")
        except BaseException as e:
            self.result.fail(
                feedback="Website does not appear to be reachable",
                staff_details={"raw": str(e), "error_type": str(type(e))},
            )
        if "The Music Shop © 2023" not in result.response.text:
            self.result.fail(feedback="Failed to detect correct content")

    def execute_web_check(self) -> None:
        """Executing web checks"""
        # Checking for music shop content
        self.content_check()
        self.music_shop_login()
        self.checking_failed_orders()
        self.checking_orders()


if __name__ == "__main__":
    MusicShopCheck().execute()
