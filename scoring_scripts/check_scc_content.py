#!/usr/bin/env python3
from .check_web_content import WebContentCheck


class SuperCoolCommunityCheck(WebContentCheck):
    def check_homepage(self) -> None:
        """Check that the homepage can be accessed and contains no errors."""
        index_page = self.driver.get(self.config.url)

        # Check if the title is there
        if not index_page.h2.text().strip().startswith("Welcome to"):
            return self.result.fail(feedback='failed to load home page')

        err_tag = index_page.main.find('b')
        if err_tag and err_tag.text().strip().lower().startswith("warning"):
            # todo change the webpage so we can parse the error here
            return self.result.warn(feedback='a warning is displayed on the webpage')

        return None

    def check_login(self) -> None:
        """Check that the site can be authenticated with."""
        try:
            login_page = self.driver.get(f"{self.config.url}/login.php")
            login_form = login_page.form
            index_page_2 = login_form.submit(
                {'username': self.config.username, 'password': self.config.password}
            )

            # Check we logged in successfully
            assert index_page_2.h2.text().strip().startswith("Welcome to")
        except Exception as e:
            return self.result.warn(
                feedback=f'User {self.config.username} failed to login to webpage',
                details={
                    'user': self.config.username,
                    'password': self.config.password,
                    'base_url': self.config.url,
                    'using_ssl': self.args.ssl
                },
                staff_details={'raw': str(e)}
            )
        return None

    def check_functionality(self) -> None:
        """Check that the site is functioning as expected by creating a post and read it again."""
        # Check the profile page and make a post
        profile_page = self.driver.get(f"{self.config.url}/me.php")
        assert profile_page.h1.text().strip() == (
            "My Profile",
            f"Loaded {self.config.url}/my.php but didn't find the header 'My Profile', has the site been manipulated?"
        )

        post_form = profile_page.find('.//div[@id="new-post"]/form')
        if not post_form:
            return self.result.warn(
                feedback="failed to find new-post form on the 'My Profile' page, has the site been manipulated?"
            )

        post_content = self.config.get_post_content()
        title = self.config.post_title
        self.result.add_detail({
            "user": self.config.username,
            "expected_title": title,
            "expected_content": post_content
        })

        index_page_2 = post_form.submit({
            "title": title,
            "text": post_content,
            "action": "post"
        })

        err_tag = index_page_2.main.find_all('div[@class="error"]')
        if err_tag:
            if len(err_tag) > 1:
                self.result.add_staff_detail("!!!WARN!!!: MULTIPLE ERROR ELEMENTS FOUND, this should never happen!")
            err_tag = err_tag[0]
            self.result.warn(
                feedback=f"failed to create post: {err_tag.text().strip()}",
                staff_feedback="This doesn't normally happen, perhaps a scoring issue? "
                               "Ex, post content is triggering an sql error? Please test manually",
                staff_details={"raw_webpage": index_page_2.html().decode("UTF-8")}
            )

        # Check if our post exists
        profile_page_url = f"{self.config.url}/page.php?u={self.config.username}"
        profile_page = self.driver.get(profile_page_url)

        found_titles = profile_page.find_all('h4')
        found = False
        for item in found_titles:
            if item.text().strip() == self.config.post_title.replace("\\", ""):
                found = True

        # special handling for this case so we can display the expected post
        try:
            assert found, "Failed to find the post I just made, what's up with this jenky website?"
        except AssertionError as exc:
            self.result.warn(feedback=str(exc), details={"url": profile_page_url})

        return None

    def execute_web_check(self) -> None:
        self.print('executing check_homepage()')
        self.check_homepage()
        self.print('executing check_login()')
        self.check_login()
        self.print('executing check_functionality()')
        self.check_functionality()


if __name__ == "__main__":
    WebContentCheck().execute()
