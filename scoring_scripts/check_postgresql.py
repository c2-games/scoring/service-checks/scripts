#!/usr/bin/env python3
import traceback
from typing import List

from pydantic import BaseModel
import psycopg2

from cyboard_check import C2GamesCheck


class C2PostgreSQL(BaseModel):
    username: str
    password: str
    database: str
    port: int
    select_query: str
    write_query: str
    write_query_values: List[str]


class PostgrSQLCheck(C2GamesCheck):
    def __init__(self):
        super().__init__()
        self.config: C2PostgreSQL = C2PostgreSQL(
            username=self.variables.check.username,
            password=self.variables.check.password,
            database=self.variables.check.db,
            port=self.variables.check.port,
            select_query=self.variables.check.select_query,
            write_query=self.variables.check.write_query,
            write_query_values=self.variables.check.write_query_values,
        )

    def check_db_read(self, cur: psycopg2.extensions.cursor) -> None:
        """Checking to see if users table is readable"""
        try:
            cur.execute(self.config.select_query)
            result = cur.fetchone()
            self.print(result)
        except BaseException as e:
            self.result.fail(
                feedback='An error occurred while trying to read from the database',
                details={
                    "username": self.config.username,
                    "password": self.config.password,
                    "port": self.config.port,
                    "database": self.config.database,
                    "host": self.host
                },
                staff_details={
                    "raw": str(e),
                    "error_type": str(type(e))
                }
            )

    def check_db_write(self, cur: psycopg2.extensions.cursor, conn: psycopg2.extensions.connection) -> None:
        """Checking to see if we can manually insert a purchase"""
        # Attempting a manual purchases addition into the DB
        try:
            values = [self.variables.jinja_render_vars(v) for v in self.config.write_query_values]
            self.print(self.config.write_query, values)
            cur.execute(self.config.write_query, values)
            conn.commit()
        except BaseException as e:
            self.result.fail(
                feedback='An error occurred while trying to write to the database',
                details={
                    "username": self.config.username,
                    "password": self.config.password,
                    "port": self.config.port,
                    "database": self.config.database,
                    "host": self.host
                },
                staff_details={
                    "raw": str(e),
                    "error_type": str(type(e))
                }
            )

    def execute(self):
        """Executing postgresql checks"""
        # Attempting to connect to the database
        try:
            conn = psycopg2.connect(f"host={self.host} \
                                      port={self.config.port} \
                                      dbname={self.config.database} \
                                      user={self.config.username} \
                                      password={self.config.password}")
            cur = conn.cursor()
        except psycopg2.OperationalError as e:
            self.print(traceback.format_exc())
            if "password authentication failed" in str(e):
                self.result.warn(
                    feedback=f"Password for user {self.config.username} failed",
                    details={
                        "username": self.config.username,
                        "password": self.config.password,
                        "port": self.config.port,
                        "database": self.config.database,
                        "host": self.host,
                        "raw": str(e)
                    },
                    staff_details={
                        "error_type": str(type(e))
                    }
                )
            else:
                self.result.fail(
                    feedback="An error was encountered while trying to connect to the database",
                    details={
                        "username": self.config.username,
                        "password": self.config.password,
                        "host": self.host,
                        "port": self.config.port,
                        "database": self.config.database
                    },
                    staff_details={
                        "raw": str(e),
                        "error_type": str(type(e))
                    }
                )
        except Exception as e:
            self.print(traceback.format_exc())
            self.result.fail(
                feedback='An error was encountered while trying to connect to the database',
                details={
                    "username": self.config.username,
                    "password": self.config.password,
                    "raw": str(e),
                    "host": self.host,
                    "port": self.config.port,
                    "database": self.config.database
                },
                staff_details={
                    "raw": str(e),
                    "error_type": str(type(e))
                }
            )

        # Attempting to check read permissions
        self.check_db_read(cur)

        # Attempting to check write permissions
        self.check_db_write(cur, conn)

        # Closing Connections
        cur.close()
        conn.close()
        # Return Success
        self.result.success(feedback="PostgreSQL connection and insert successful")


if __name__ == '__main__':
    PostgrSQLCheck().execute()
