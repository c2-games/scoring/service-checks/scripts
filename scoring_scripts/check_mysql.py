#!/usr/bin/env python3
import random
import hashlib
import traceback
from typing import Optional, Dict

from pydantic import BaseModel
import mysql.connector

from cyboard_check import C2GamesCheck


class C2MYSQL(BaseModel):
    username: str  # = sql_user
    password: str  # = sql_password
    db_name: str  # = sql_db
    table_name: str  # = sql_test_table
    # Key is SQL column, value is test data to insert
    test_data: Dict[str, str]

    @property
    def table_ins(self):
        """Return MySQL Variable Interpolation String for data."""
        # todo support more than only string test data
        return ",".join('%s' for _ in self.test_data.keys())

    @property
    def query(self):
        """Return a formatted query that can be passed to MySQL based on Test Data."""
        return f"INSERT INTO {self.table_name} ({', '.join(self.test_data.keys())}) VALUES ({self.table_ins})"


class MySqlCheck(C2GamesCheck):
    def __init__(self):
        super().__init__()
        self.config: C2MYSQL = C2MYSQL(
            username=self.variables.check.sql_user,
            password=self.variables.check.sql_password,
            db_name=self.variables.check.sql_db,
            table_name=self.variables.check.sql_test_table,
            test_data=self.variables.check.sql_test_data,
        )
        self.print(self.config.json(indent=2))

    @classmethod
    def get_unique_id(cls) -> str:
        """Get a unique MD5 string."""
        return str(hashlib.md5(str(random.randint(0, 999999999)).encode('utf-8')).hexdigest())

    def get_staff_error(self, exc: Optional[Exception] = None, error: Optional[str] = None):
        """Create a staff error object with the exception/error string and execution information."""
        ret = self.config.dict()
        if exc:
            ret['raw'] = str(exc)
        if error:
            ret['error'] = error
        return ret

    def execute(self):
        """Execute the Check."""
        try:
            # Check initial connection
            db = mysql.connector.connect(
                host=self.host,
                user=self.config.username,
                password=self.config.password,
                database=self.config.db_name,
                connection_timeout=self.args.timeout
            )
        except mysql.connector.errors.ProgrammingError as e:
            self.print(traceback.format_exc())
            # this case can happen when the user can't access something
            # ex, the scoring user no longer has insert permissions to the required table
            return self.result.warn(
                feedback='Failed to access required data', details={
                    'user': self.config.username,
                    'password': self.config.password,
                    'raw': str(e),
                    'host': self.host,
                    'dbName': self.config.db_name,
                    'tableName': self.config.table_name,
                },
                staff_details=self.get_staff_error(e, "Failed to execute query")
            )
        except (mysql.connector.errors.InterfaceError, mysql.connector.errors.DatabaseError) as e:
            self.print(traceback.format_exc())
            return self.result.fail(
                feedback='Failed to connect to MySQL host', details={
                    'user': self.config.username,
                    'password': self.config.password,
                    'raw': str(e),
                    'host': self.host,
                    'dbName': self.config.db_name,
                    'tableName': self.config.table_name,
                },
                staff_details=self.get_staff_error(e, "Failed to connect to MySQL System")
            )

        try:
            # Check Tables Read
            dbc = db.cursor()

            # If it fails after this line it is a table read error
            self.result.feedback = f"Failed to read from table {self.config.table_name}"

            dbc.execute(f"SELECT * FROM {self.config.table_name}")
            _ = dbc.fetchall()  # called for side effects

            # If it fails after this line it is a table write error
            self.result.feedback = f"Failed to write to table {self.config.table_name}"

            query = self.config.query
            data = [self.variables.jinja_render_vars(x) for x in self.config.test_data.values()]
            self.print('query:', query)
            self.print('values:', data)
            dbc.execute(query, data)
            db.commit()

            return self.result.success(feedback="MySQL Connection and Table Access Successful!")
        except mysql.connector.errors.DataError as e:
            return self.result.warn(
                details={'raw': str(e)},
                staff_details=self.get_staff_error(e, "Data error in MySQL. Check table fields.")
            )
        except mysql.connector.errors.ProgrammingError as e:
            # this case can happen when the user can't access something
            # ex, the scoring user no longer has insert permissions to the required table
            return self.result.warn(
                feedback='Failed to access required data', details={
                    'user': self.config.username,
                    'password': self.config.password,
                    'raw': str(e),
                    'host': self.host,
                    'dbName': self.config.db_name,
                    'tableName': self.config.table_name,
                },
                staff_details=self.get_staff_error(e, "Failed to execute query")
            )
        finally:
            db.close()


if __name__ == '__main__':
    MySqlCheck().execute()
