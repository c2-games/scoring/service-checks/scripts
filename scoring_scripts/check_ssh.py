#!/usr/bin/env python3
import hashlib
from io import StringIO
import socket
from typing import IO
from random import sample

import paramiko
import paramiko.ssh_exception

from cyboard_check import C2GamesCheck


class SshCheck(C2GamesCheck):
    @classmethod
    def get_digest(cls, file_obj: IO) -> str:
        """Get SHA1 Digest of an IO object"""
        file_hash = hashlib.sha1()
        while True:
            # Reading is buffered, so we can read smaller chunks.
            chunk = file_obj.read(file_hash.block_size)
            if not chunk:
                break
            file_hash.update(chunk.encode())
        return file_hash.hexdigest()

    @classmethod
    def get_digest_file(cls, file_path) -> str:
        """Get digest of file on the system"""
        with open(file_path, 'rb') as file:
            return cls.get_digest(file)

    def execute(self):
        """Execute the Check."""
        failed_users = []
        key_file = StringIO(self.variables.check.ssh_key)
        key = paramiko.RSAKey.from_private_key(key_file)
        key_file.seek(0)
        key_digest = self.get_digest(key_file)
        # Create Paramiko SSH client and ignore missing host key
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_users = self.variables.check.ssh_users
        user_limit = 10

        if len(ssh_users) > user_limit:
            ssh_users = sample(ssh_users, user_limit)

        for user in ssh_users:
            try:
                # Open connection with priv key
                ssh.connect(hostname=self.host, username=user, pkey=key, timeout=self.args.timeout)
                ssh.exec_command("exit 0")
            except (paramiko.ssh_exception.NoValidConnectionsError, socket.timeout) as e:
                self.result.fail(
                    feedback=f"Failed to connect to host: {self.host}",
                    details={
                        'raw': str(e)
                    },
                    staff_details={
                        'user': user,
                        'host': self.host,
                        'timeout': self.args.timeout,
                        'key_sha1': key_digest
                    }
                )
            except paramiko.AuthenticationException as e:
                failed_users.append(user)
                self.result.add_staff_detail({
                    'user': user,
                    'error': 'Authentication Failed',
                    'raw': str(e)
                })
            except Exception as exc:
                part_feedback = {
                    'raw': str(exc),
                    'user': user,
                    'host': self.host
                }
                self.result.add_detail(part_feedback)
                self.result.add_staff_detail({
                    **part_feedback,
                    'key_sha1': key_digest,
                    'password': None
                })
            finally:
                ssh.close()

        if failed_users:
            self.result.add_detail({
                'failed_users': failed_users
            })
            self.result.warn(
                feedback=f"The following users failed to authenticate "
                         f"with their public key: {', '.join(failed_users)}",
                details={
                    'host': self.host
                },
                staff_details={
                    'key_sha1': key_digest,
                }
            )

        return self.result.success(feedback="SSH Successful!")


if __name__ == "__main__":
    SshCheck().execute()
