#!/usr/bin/env python3
import argparse
import time
from typing import Optional, Dict, Any

from pydantic import BaseModel
import psycopg2

from cyboard_check import C2GamesCheck


class C2PSQL(BaseModel):
    username: str
    password: str
    db_name: str
    port: int
    table_name: str
    # Key is column, value is test data to insert
    test_data: list
    batch_size: int


class PostgreSqlInject(C2GamesCheck):
    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Connect to web and check if HTTP content can be retrieved.'
        )
        parser.add_argument('-s', '--sleep', type=int, default=2, help='sleep time (seconds) between inserts')
        parser.add_argument('-b', '--batch-size', type=int, help='override number of users inserted at one time')
        super().__init__(parser=parser)
        self.config: C2PSQL = C2PSQL(
            username=self.variables.check.user,
            password=self.variables.check.password,
            db_name=self.variables.check.db,
            port=self.variables.check.port,
            table_name=self.variables.check.test_table,
            test_data=self.variables.check.test_data,
            batch_size=self.args.batch_size or self.variables.check.batch_size
        )
        self.print(self.config.json(indent=2))

    def multi_query(self, users):
        """Build query for multiple values."""
        query: Dict[str, Any] = {}
        query['values'] = []
        query_values = ""

        for val in users:
            query_values += f"""({', '.join('%s' for v in val.values())}), """
            for x in val.values():
                query['values'].append(x)
        query_values = query_values[:-2]
        query['query'] = ' '.join([
            f"INSERT INTO {self.config.table_name}",
            f"({', '.join(self.config.test_data[0].keys())}) VALUES ",
            query_values,
            ' ON CONFLICT DO NOTHING;'
        ])

        return query

    def get_staff_error(self, exc: Optional[Exception] = None, error: Optional[str] = None):
        """Create a staff error object with the exception/error string and execution information."""
        ret = self.config.dict()
        if exc:
            ret['raw'] = str(exc)
        if error:
            ret['error'] = error
        return ret

    def execute(self):
        """Execute the Check."""
        try:
            # Check initial connection
            db = psycopg2.connect(f"host={self.host} \
                                    user={self.config.username} \
                                    password={self.config.password} \
                                    dbname={self.config.db_name} \
                                    port={self.config.port} \
                                   ")
        except psycopg2.ProgrammingError as e:
            # this case can happen when the user can't access something
            # ex, the scoring user no longer has insert permissions to the required table
            return self.result.warn(
                feedback='Failed to access required data', details={
                    'user': self.config.username,
                    'password': self.config.password,
                    'raw': str(e),
                    'host': self.host,
                    'dbName': self.config.db_name,
                    'tableName': self.config.table_name,
                },
                staff_details=self.get_staff_error(e, "Failed to execute query")
            )
        except psycopg2.InterfaceError as e:
            return self.result.fail(
                feedback='Failed to connect to PostgreSQL host', details={
                    'user': self.config.username,
                    'password': self.config.password,
                    'raw': str(e),
                    'host': self.host,
                    'dbName': self.config.db_name,
                    'tableName': self.config.table_name,
                },
                staff_details=self.get_staff_error(e, "Failed to connect to PostgreSQL System")
            )
        except Exception as e:
            return self.result.fail(
                staff_feedback='Unknown error occurred',
                staff_details={
                    "raw": str(e)
                }
            )

        try:
            # Check Tables Read
            dbc = db.cursor()

            # If it fails after this line it is a table create error
            self.result.feedback = "Failed to generate table"
            # TODO :: fix this so it is not hardcoded
            # pylint: disable=line-too-long
            dbc.execute("CREATE TABLE IF NOT EXISTS migrate_users (user_id serial PRIMARY KEY UNIQUE, username text NOT NULL UNIQUE, first text NOT NULL, last text NOT NULL, password_hash text NOT NULL);")
            db.commit()

            # If it fails after this line it is a table read error
            self.result.feedback = f"Failed to read from table {self.config.table_name}"

            dbc.execute(f"SELECT * FROM {self.config.table_name};")
            _ = dbc.fetchall()  # called for side effects

            # If it fails after this line it is a table write error
            self.result.feedback = f"Failed to write to table {self.config.table_name}"

            # Temporary variable to build user inserts up to ${self.config.batch_size}
            # at a time users
            user_list = []
            # Array of queries to be performed later on in the scoring
            # script
            query_list = []

            # Building out the query_list by grouping user inserts
            for index, username in enumerate(self.config.test_data):
                user_list.append(username)
                if len(user_list) == self.config.batch_size or index == (len(self.config.test_data) - 1):
                    query_list.append(self.multi_query(user_list))
                    user_list = []

            # Executing all queries built above
            for query in query_list:
                try:
                    dbc.execute(query['query'], query['values'])
                    db.commit()
                except Exception as e:
                    self.print(e)
                    self.print(f"error in::{query}")
                # Sleeping for 2.5 seconds to slow the insertion of data
                time.sleep(self.args.sleep)

            return self.result.success(feedback="Scoring scripts has run through all inserts")
        except psycopg2.DataError as e:
            return self.result.warn(
                details={'raw': str(e)},
                staff_details=self.get_staff_error(e, "Data error in PostgreSQL. Check table fields.")
            )
        except psycopg2.ProgrammingError as e:
            # this case can happen when the user can't access something
            # ex, the scoring user no longer has insert permissions to the required table
            return self.result.warn(
                feedback='Failed to access required data', details={
                    'user': self.config.username,
                    'password': self.config.password,
                    'raw': str(e),
                    'host': self.host,
                    'dbName': self.config.db_name,
                    'tableName': self.config.table_name,
                },
                staff_details=self.get_staff_error(e, "Failed to execute query")
            )
        except Exception as e:
            return self.result.fail(
                staff_feedback='Unknown error occurred',
                staff_details={
                    "raw": str(e)
                }
            )
        finally:
            db.close()


if __name__ == '__main__':
    PostgreSqlInject().execute()
