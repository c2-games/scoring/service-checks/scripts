#!/usr/bin/env python3
# this failed to import as a relative import when the script is run directly,
# but fails to import for pylint when _not_ a relative import. So, we just ignore the error within pylint.
# pylint: disable=import-error
import check_web_content


class BetterBlogCheck(check_web_content.WebContentCheck):

    def _has_php_warning(self, page) -> bool:
        """Checking for php warning on page"""
        bold_tags = page.find_all('b')
        for bold_tag in bold_tags:
            if bold_tag and bold_tag.text().strip().lower().startswith("warning"):
                return True
        return False

    def check_homepage(self) -> None:
        """Check that the homepage can be accessed and contains no errors."""
        index_page = self.driver.get(self.config.url)

        # Check if the title is there
        if not index_page.h1.text().strip().startswith(f"{self.variables.check.homepage_title}"):
            return self.result.warn(feedback='failed to load home page', details={
                'expected_page_title': self.variables.check.homepage_title,
                'actual_page_title': index_page.h1.text().strip(),
            })

        has_php_error = self._has_php_warning(index_page)
        if has_php_error:
            return self.result.warn(feedback='A warning is displayed on the index page')

        return None

    def check_login(self) -> None:
        """Check that the site can be authenticated with."""
        try:
            admin_page = self.driver.get(f"{self.config.http_auth_url}/admin.php")

            assert b"Not authenticated" not in admin_page.html()

            # Check we logged in successfully
            assert admin_page.strong.text().strip().startswith("Hello, " + self.config.username)
        except Exception as e:
            return self.result.warn(
                feedback=f'User {self.config.username} failed to login to webpage with HTTP authentication',
                details={
                    'user': self.config.username,
                    'password': self.config.password,
                    'base_url': self.config.url,
                    'using_ssl': self.args.ssl
                },
                staff_details={'raw': str(e)}
            )
        return None

    def check_functionality(self) -> None:
        """Check that the site is functioning as expected by creating a post and read it again."""

        post_content = self.config.get_post_content()
        title = self.variables.jinja_render_vars(self.config.post_title)
        filename = title.replace(" ", "_") + ".php"
        self.result.add_detail({
            "user": self.config.username,
            "expected_title": title,
            "expected_content": post_content
        })

        # Check the profile page and make a post
        posts_page = self.driver.get(f"{self.config.http_auth_url}/admin.php?action=posts&post={filename}")

        try:
            assert posts_page.h2.text().strip() == "Posts"
        except Exception as e:
            return self.result.warn(
                feedback=f'Loaded {self.config.url}/admin.php?action=posts but didn\'t find the \
                           header \'Posts\', has the site been manipulated?',
                details={
                    'user': self.config.username,
                    'password': self.config.password,
                    'base_url': self.config.url,
                    'using_ssl': self.args.ssl
                },
                staff_details={'raw': str(e)}
            )

        post_data_form = posts_page.form
        if not post_data_form:
            return self.result.warn(
                feedback=f'Loaded {self.config.url}/admin.php?action=posts&post={filename} but didn\'t find the form.',
                details={
                    'user': self.config.username,
                    'password': self.config.password,
                    'base_url': self.config.url,
                    'using_ssl': self.args.ssl
                }
            )

        post_data_form.submit({
            "title": title,
            "subtitle": "This is filing " + title,
            "description": "",
            "path": title,
            "action": "savepost",
            "postcontent": post_content
        })

        post_url = f"{self.config.http_auth_url}/index.php?post={title}"
        self.print(f'fetching {post_url}')
        response = self.driver.get(post_url)
        self.print(f"retrieved post status={response.last_response.status_code}")

        posts = response.find_all('div[@class="media-content"]')
        found = False
        found_subtitle = False

        for post in posts:
            title_ele = post.find('.//p[@class="title is-2"]')
            if title_ele and title_ele.text().strip() == title.replace("\\", ""):
                found = True
                subtitle_tag = post.find('.//p[@class="subtitle is-6"]')

                if title.replace("\\", "") in subtitle_tag.text().strip():
                    found_subtitle = True

        # special handling for this case so we can display the expected post
        try:
            assert found, "Failed to find the post I just made, what's up with this jenky website?"
        except AssertionError as exc:
            self.result.warn(feedback=str(exc), details={"url": f"{self.config.http_auth_url}/index.php"})

        try:
            assert found_subtitle, "Found my the post I made but something \
                                    looks off, what's going on? Is the database on?"
        except AssertionError as exc:
            self.result.warn(feedback=str(exc), details={"url": f"{self.config.http_auth_url}/index.php"})

        return None

    def execute_web_check(self) -> None:
        """Execute Betterblog web check"""
        self.print('executing check_homepage()')
        self.check_homepage()
        self.print('executing check_login()')
        self.check_login()
        self.print('executing check_functionality()')
        self.check_functionality()


if __name__ == "__main__":
    BetterBlogCheck().execute()
