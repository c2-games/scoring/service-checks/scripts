#!/usr/bin/env python3
import time
from typing import Optional, Dict, Any

from pydantic import BaseModel
import mysql.connector

from cyboard_check import C2GamesCheck


class C2MYSQL(BaseModel):
    username: str  # = sql_user
    password: str  # = sql_password
    db_name: str  # = sql_db
    table_name: str  # = sql_test_table
    # Key is SQL column, value is test data to insert
    test_data: list

    @property
    def table_ins(self):
        """Return MySQL Variable Interpolation String for data."""
        # todo support more than only string test data
        return ",".join('%s' for _ in self.test_data[0].keys())

    @property
    def query(self):
        """Return a formatted query that can be passed to MySQL based on Test Data."""
        return f"INSERT IGNORE INTO {self.table_name} ({', '.join(self.test_data[0].keys())}) VALUES ({self.table_ins})"


class MySqlInject(C2GamesCheck):
    def __init__(self):
        super().__init__()
        self.config: C2MYSQL = C2MYSQL(
            username=self.variables.check.sql_user,
            password=self.variables.check.sql_password,
            db_name=self.variables.check.sql_db,
            table_name=self.variables.check.sql_test_table,
            test_data=self.variables.check.sql_test_data,
        )
        self.print(self.config.json(indent=2))

    def multi_query(self, users):
        """Build query for multiple values."""
        query: Dict[str, Any] = {}
        query['values'] = []
        query_values = ""

        for val in users:
            query_values += f"""({', '.join('%s' for v in val.values())}), """
            for x in val.values():
                query['values'].append(x)
        query_values = query_values[:-2]
        # pylint: disable-next=line-too-long
        query['query'] = f"INSERT IGNORE INTO {self.config.table_name} ({', '.join(self.config.test_data[0].keys())}) VALUES " + query_values

        return query

    def get_staff_error(self, exc: Optional[Exception] = None, error: Optional[str] = None):
        """Create a staff error object with the exception/error string and execution information."""
        ret = self.config.dict()
        if exc:
            ret['raw'] = str(exc)
        if error:
            ret['error'] = error
        return ret

    def execute(self):
        """Execute the Check."""
        try:
            # Check initial connection
            db = mysql.connector.connect(
                host=self.host,
                user=self.config.username,
                password=self.config.password,
                database=self.config.db_name
            )
        except mysql.connector.errors.ProgrammingError as e:
            # this case can happen when the user can't access something
            # ex, the scoring user no longer has insert permissions to the required table
            return self.result.warn(
                feedback='Failed to access required data', details={
                    'user': self.config.username,
                    'password': self.config.password,
                    'raw': str(e),
                    'host': self.host,
                    'dbName': self.config.db_name,
                    'tableName': self.config.table_name,
                },
                staff_details=self.get_staff_error(e, "Failed to execute query")
            )
        except mysql.connector.errors.InterfaceError as e:
            return self.result.fail(
                feedback='Failed to connect to MySQL host', details={
                    'user': self.config.username,
                    'password': self.config.password,
                    'raw': str(e),
                    'host': self.host,
                    'dbName': self.config.db_name,
                    'tableName': self.config.table_name,
                },
                staff_details=self.get_staff_error(e, "Failed to connect to MySQL System")
            )

        try:
            # Check Tables Read
            dbc = db.cursor()

            # If it fails after this line it is a table read error
            self.result.feedback = f"Failed to read from table {self.config.table_name}"

            dbc.execute(f"SELECT * FROM {self.config.table_name}")
            _ = dbc.fetchall()  # called for side effects

            # If it fails after this line it is a table write error
            self.result.feedback = f"Failed to write to table {self.config.table_name}"

            # Temporary variable to build user inserts up to 10 users
            # at a time
            user_list = []
            # Array of queries to be performed later on in the scoring
            # script
            query_list = []

            # Building out the query_list by grouping user inserts
            for index, user in enumerate(self.config.test_data):
                user_list.append(user)
                if len(user_list) == 10 or index == (len(self.config.test_data) - 1):
                    query_list.append(self.multi_query(user_list))
                    user_list = []

            # Executing all queries built above
            for query in query_list:
                try:
                    # This is auto-reconnect if the services goes down in between inserts
                    # It also means a few groups in inserts can fail and then get picked up
                    #   later on in execution
                    db.ping(reconnect=True)
                    dbc.execute(query['query'], query['values'])
                    db.commit()
                except Exception as e:
                    self.print(e)
                    self.print(f"error in::{query}")
                # Sleeping for 2.5 seconds to slow the insertion of data
                time.sleep(2.5)

            return self.result.success(feedback="Scoring scripts has run through all inserts")
        except mysql.connector.errors.DataError as e:
            return self.result.warn(
                details={'raw': str(e)},
                staff_details=self.get_staff_error(e, "Data error in MySQL. Check table fields.")
            )
        except mysql.connector.errors.ProgrammingError as e:
            # this case can happen when the user can't access something
            # ex, the scoring user no longer has insert permissions to the required table
            return self.result.warn(
                feedback='Failed to access required data', details={
                    'user': self.config.username,
                    'password': self.config.password,
                    'raw': str(e),
                    'host': self.host,
                    'dbName': self.config.db_name,
                    'tableName': self.config.table_name,
                },
                staff_details=self.get_staff_error(e, "Failed to execute query")
            )
        finally:
            db.close()


if __name__ == '__main__':
    MySqlInject().execute()
