#!/usr/bin/env python3
import argparse
import traceback

import dns.reversename
import dns.resolver
import dns.exception

from cyboard_check import C2GamesCheck
from pydantic import Field


class DnsCheck(C2GamesCheck):
    resolver: dns.resolver.Resolver = Field(default_factory=dns.resolver.Resolver)

    def __init__(self):
        parser = argparse.ArgumentParser()

        parser.add_argument(
            '-r', '--reverse', action='store_true', help='do reverse lookups'
        )
        parser.add_argument(
            '-f', '--forward', action='store_true', help='do forward lookups'
        )

        super().__init__(parser=parser)
        self.resolver.nameservers = [self.host]

        if not (self.args.forward or self.args.reverse):
            raise RuntimeError('must specify at-least one of --forward / --reverse')

    def get_staff_error(self, causes, error, domain):
        """
        Format the inputs into a dict for use in the staff details
        """
        return {
            "causes": causes,
            "raw": error,
            "domain": domain
        }

    def check_rev(self, domain, ip) -> bool:
        """
        Do a reverse lookup. Return True if found, False, if failed
        """
        ans = self.resolver.resolve(dns.reversename.from_address(ip), 'PTR', lifetime=self.args.timeout)
        for x in ans:
            lookup = str(x).rstrip('.')  # The PTR has the trailing '.'
            if lookup == domain:
                return True
        return False

    def check_forward(self, domain, ip) -> bool:
        """
        Do a forward lookup. Return True if found, False, if failed
        """
        ans = self.resolver.resolve(domain, lifetime=self.args.timeout)
        return str(ans[0]) == ip

    def get_domains(self):
        """Get Domain Names from Variables."""
        domains = {}
        template_domains = self.variables.check.domains
        for k, v in template_domains.items():
            k = self.variables.jinja_render_vars(k)
            v = self.variables.jinja_render_vars(v)
            domains[k] = v
        return domains

    def execute(self):
        """Execute the Check."""
        domains = self.get_domains()
        self.print(f'checking {self.host} for {domains}')
        failed_lookups = []
        errors = []

        for domain, ip in domains.items():
            # Just here to make the unbound errors go away
            lookup, target = "unspecified", "unspecified"
            try:
                if self.args.forward:
                    lookup, target = domain, ip
                    success = self.check_forward(domain, ip)
                    if not success:
                        failed_lookups.append(lookup)
                        errors.append(f"Incorrect or missing lookup: {lookup} -> {target}")
                if self.args.reverse:
                    lookup, target = ip, domain
                    success = self.check_rev(domain, ip)
                    if not success:
                        failed_lookups.append(lookup)
                        errors.append(f"Incorrect or missing lookup: {lookup} -> {target}")
            except dns.resolver.NXDOMAIN:
                self.print(traceback.format_exc())
                failed_lookups.append(lookup)
                errors.append(f"Failed Lookup (NXDOMAIN): {lookup} -> {target}")
            except dns.exception.Timeout as e:
                self.print(traceback.format_exc())
                errors.append(f"Could not reach DNS server: {e}")
                self.result.fail(feedback=f"Can't contact DNS Server on {self.host}", details=errors)
            except dns.resolver.NoNameservers as e:
                self.print(traceback.format_exc())
                errors.append(f"No DNS response: {domain}")
                self.result.add_staff_detail(self.get_staff_error(
                    "Check to see if zone file is loaded", str(e), domain
                ))
                failed_lookups.append(domain)
            except dns.exception.DNSException as e:
                self.print(traceback.format_exc())
                errors.append(f"Malformed DNS Request: {e}")
                self.result.add_staff_detail(self.get_staff_error(
                    "Unhandled DNS exception", str(e), domain
                ))
                failed_lookups.append(domain)

        if errors:
            self.result.add_detail(errors)

        if failed_lookups:
            if len(failed_lookups) == len(domains.keys()):
                self.result.warn(feedback=f"Connected to {self.host}:53, no useful content though...")
            else:
                self.result.warn(feedback=f"Failed to lookup: {', '.join(failed_lookups)}")

        self.result.success(feedback="All hosts successfully found!")


if __name__ == "__main__":
    DnsCheck().execute()
