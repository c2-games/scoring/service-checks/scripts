#!/usr/bin/env python3
from uuid import uuid4
from random import randint

# this failed to import as a relative import when the script is run directly,
# but fails to import for pylint when _not_ a relative import. So, we just ignore the error within pylint.
# pylint: disable=import-error
import check_web_content


class ETechAcademy(check_web_content.WebContentCheck):
    def check_add_student_account(self) -> None:
        """Attempt to create student account"""
        user_data = {
            "username": f"student_{uuid4()}",
            "password": f"password_{randint(1000, 9999)}",
            "role": "student",
        }

        response = None
        try:
            response = self.driver.session.post(
                f"{self.config.url}/admin_dashboard/add_user",
                data=user_data,
                allow_redirects=True,
                timeout=1
            )
            response.raise_for_status()
        except Exception as e:
            msg = f"{self.config.username} was not able to create the student user {user_data['username']}"
            feedback = {'raw': str(e)}
            if response:
                feedback['http_status'] = response.status_code
                msg += f" (http_status={response.status_code})"

            self.result.warn(
                feedback=msg,
                staff_feedback="failed to create student account",
                staff_details={
                    **feedback,
                    'cookies': self.driver.session.cookies.items(),
                    'user_data': user_data,
                },
            )

    def etech_academy_login(self) -> None:
        """Login with configured credentials"""
        login_info = {
            "username": self.config.username,
            "password": self.config.password,
        }

        response = None
        try:
            response = self.driver.session.post(
                f"{self.config.url}/login.html",
                data=login_info,
                allow_redirects=True,
                timeout=1
            )
            response.raise_for_status()
        except BaseException as e:
            msg = f"{self.config.username} was not able to request the login page"
            if response:
                msg += f" (http_status={response.status_code})"

            self.result.fail(
                feedback=msg,
                staff_details={"raw": str(e), "error_type": str(type(e))},
            )
        response_domains = self.driver.session.cookies.list_domains()

        # this handles if we're redirected to teamX.ncaecybergames.org instead of the IP address
        if response_domains:
            self.config.host = response_domains[0]

        if "session_id" not in self.driver.session.cookies:
            self.result.fail(
                feedback=f"{self.config.username} was unable to login",
                details={
                    "username": self.config.username,
                    "password": self.config.password,
                    "base_url": self.config.url,
                    "using_ssl": self.config.ssl
                },
                staff_feedback="session_id cookie not found in session",
                staff_details={
                    "user": self.config.username,
                    "pass": self.config.password,
                    "raw": "session token was not generated",
                    "cookies": f"{self.driver.session.cookies.items()}",
                },
            )

    def check_content(self) -> None:
        """Checking for eTech Academy Copyright Message"""
        response = None
        try:
            response = self.driver.session.get(f"{self.config.url}/index.html")
            response.raise_for_status()
        except Exception as e:
            msg = "Website cannot be reached"
            if response:
                msg += f" (http_status={response.status_code})"

            self.result.fail(
                feedback=msg,
                staff_details={"raw": str(e)},
            )

        if "&copy; 2024 eTech Academy" not in response.text:
            self.result.fail(feedback="Failed to detect correct content")

    def execute_web_check(self) -> None:
        """Executing web checks"""
        self.print('checking content')
        self.check_content()
        self.print('checking login')
        self.etech_academy_login()
        self.print('adding student account')
        self.check_add_student_account()


if __name__ == "__main__":
    ETechAcademy().execute()
