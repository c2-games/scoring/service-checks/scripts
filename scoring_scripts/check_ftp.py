#!/usr/bin/env python3
from ftplib import FTP, all_errors as FTPError
import re
import argparse
import io
import random
import hashlib
from typing import Optional, Dict, Tuple, List, Generator

from pydantic import BaseModel, root_validator
from pydantic.class_validators import validator

from cyboard_check import C2GamesCheck


class FTPFileList(BaseModel):
    name: str
    hash: Optional[str]
    size: Optional[str]

    @validator('size')
    def size_is_str(cls, v):  # pylint: disable=no-self-argument
        """Convert size to a str if it's populated. We get file sizes as strings from the FTP client."""
        if v:
            return str(v)
        return v


class C2FTP(BaseModel):
    username: str
    password: str
    users: Dict[str, str]
    mount_location: str
    files: List[FTPFileList]

    @property
    def exec_data(self):
        """Return data used during execution for reporting purposes."""
        return {
            "username": self.username,
            "password": self.password,
            "mount_location": self.mount_location
        }

    @root_validator(pre=True)
    # pylint: disable=no-self-argument
    def populate_user_and_pass(cls, values):  # pylint: disable=no-self-argument
        """Populates username and password from user list if not already populated."""
        username = values.get("username")
        password = values.get("password")
        users = values.get("users")
        if not users:
            # If users doesn't exist, we can't do anything, so just return
            return values

        random_user = random.choice(list(users.items()))
        if not (username and password):
            # If both aren't defined, then set them both. Otherwise, handle normally
            values["username"], values["password"] = random_user
            return values

        return values

    def _select_random_user(self):
        """Selects a random username/password to use for the check"""
        random.choice(list(self.users.items()))


class FTPCheck(C2GamesCheck):
    files: Dict[str, str] = {}

    def __init__(self):
        # Setup custom Parser
        parser = argparse.ArgumentParser(description='Connect to FTP and check for some files.')
        parser.add_argument('-d', '--debug', action='store_true', help='print debug messages to stderr')
        parser.add_argument('-s', '--check-size', action='store_true', help='check file size of expected files')
        parser.add_argument('-H', '--check-hash', action='store_true', help='check file hash of a single expected file')
        parser.add_argument('-l', '--check-login', action='store_true', help='checks if login is successful')
        parser.add_argument('-w', '--check-write', action='store_true', help='checks if writes succeed')

        # C2GamesCheck will parse arguments and load variables file
        super().__init__(parser=parser)

        # After everything is initialized, set up our custom config
        self.config: C2FTP = C2FTP(
            users=self.variables.check.ftp_users,
            mount_location=self.variables.check.ftp_mount_location,
            files=self.variables.check.ftp_files
        )

    def get_staff_error(self, exc: Optional[Exception] = None, error: Optional[str] = None):
        """Create a staff error object with the exception/error string and execution information."""
        ret = self.config.exec_data
        if exc:
            ret['raw'] = str(exc)
        if error:
            ret['error'] = error
        return ret

    def parse_line(self, line):
        """Parse a line returned from retrlines(LIST) for filename and filesize."""
        reg = re.compile(r'..........\s+[0-9] \S+\s+\S+\s+([0-9]+) [a-zA-Z]+ [0-9]+ [0-9]+:[0-9]+ (.+)')
        match = reg.match(line)
        if not match:
            self.print(f'ERROR: Invalid line found: {line}')
            return

        self.files[match.group(2)] = match.group(1)

    def write_file_check(self, ftp: FTP):
        """Attempt to write a file to the remote system"""
        self.print("writing files and checking results")
        try:
            f = io.BytesIO(b"Scoring file do not remove")
            filename = str(hashlib.sha256(str(random.randint(0, 999999999)).encode('utf-8')).hexdigest()) + ".txt"
            ftp.storlines("STOR " + filename, f)
            ftp.delete(filename)
            self.result.feedback = f"FTP write working on host {self.host}"
        except Exception as e:
            return self.result.warn(
                feedback=f"FTP write error on host {self.host}", details={'raw': str(e)},
                staff_details=self.get_staff_error(e, "FTP Write Failure")
            )
        return self.result.success(feedback=f"FTP write successful on host {self.host}")

    def _iterate_files(self) -> Generator[Tuple[str, Optional[str], Optional[str]], None, None]:
        for file in self.config.files:
            yield file.name, file.size, file.hash

    def read_file_check(self, ftp: FTP):
        """Attempt to read files on the remote system and verify them using either a hash or file size"""
        self.print("reading files and checking results")
        missing = []
        wrong = []
        for filename, size, _ in self._iterate_files():
            self.print("performing filesize checks and populating missing files")
            if not size:
                continue

            if filename not in self.files:
                missing.append(filename)
                self.print(f'ERROR: required file not found: {filename}')
                continue
            if self.args.check_size and self.files[filename] != size:
                wrong.append(filename)
                self.print(f'ERROR: required file was the wrong size: {filename}')
                continue
            self.print(f'file was ok: {filename}\t{size}')

        self.print("missing files", missing)
        self.print("wrong sized files", wrong)

        if self.args.check_hash:
            self.print("performing hash checks")
            for retr_file, _, expected_hash in self._iterate_files():
                if not expected_hash or retr_file in missing:
                    continue

                file_hash = hashlib.md5()
                try:
                    ftp.retrbinary(f"RETR {retr_file}", file_hash.update)
                    self.print(f"{retr_file}: {file_hash.hexdigest()} =?= {expected_hash}")
                    if file_hash.hexdigest() != expected_hash:
                        raise Exception(f"hash != expected value: {file_hash.hexdigest()} != {expected_hash}")
                except Exception as e:
                    wrong.append(retr_file)
                    self.print(f'failed to hash the file {retr_file}')
                    self.result.add_staff_detail(self.get_staff_error(e, f"failed to hash file {retr_file}"))

        ftp.close()

        if missing and wrong:
            msg = 'Files are both missing and failed the integrity check'
        elif missing:
            msg = f'{len(missing)} files are missing'
        elif wrong:
            msg = f'{len(wrong)} files failed the integrity check'
        else:
            return self.result.success(
                feedback=f'FTP Content present and passing integrity checks on host {self.host}!'
            )

        return self.result.warn(
            feedback=f"{msg} on host {self.host} as {self.config.username}",
            details={
                'missing_files': missing,
                'incorrect_files': wrong,
            }
        )

    def check_all_logins(self):
        """Checks that all supplied users can authenticate with the FTP Server."""
        failed_users = []

        # Check login for all users
        self.print("checking logins")
        for user, password in self.config.users.items():
            self.print(f"checking login {user}:{password}")
            try:
                # todo don't just use the last user logged in?
                ftp_login = FTP(self.host)
                ftp_login.login(user, password)
            except FTPError as e:
                failed_users.append(user)
                self.result.add_detail({
                    'user': user,
                    'reason': str(e)
                })
            except Exception as e:
                self.print(f"Failed to authenticate to FTP with user {self.config.username} on host {self.host}")
                failed_users.append(user)
                self.result.add_staff_detail(self.get_staff_error(e, error="failed to authenticate"))

        if failed_users:
            # failed to authenticate, bail early
            self.result.fail(feedback=f"The following users failed to authenticate with FTP: {', '.join(failed_users)}")

    def execute(self):
        """Execute the Check."""
        self.print(f"using creds '{self.config.username}:{self.config.password}@{self.host}'")

        try:
            self.print("checking connection")
            ftp = FTP(self.host)
        except Exception as e:
            self.print(f'failed to connect to host: {e}')
            return self.result.fail(
                feedback=f"Failed to connect to {self.host} over FTP",
                staff_details=self.get_staff_error(e)
            )

        self.check_all_logins()

        # Stop here if login check is called
        self.result.feedback = "Login Successful"
        if self.args.check_login:
            ftp.close()
            return self.result.success()

        # Login as a specific user for the next set of tests
        try:
            ftp.login(self.config.username, self.config.password)
            ftp.cwd(self.config.mount_location)
            ftp.retrlines('LIST', callback=self.parse_line)
        except FTPError as e:
            return self.result.warn(
                feedback=f"Failed to retrieve data from host {self.host} as user {self.config.username}",
                details={'error': str(e)},
                staff_details=self.get_staff_error(e, "failed to get file listing (delayed failed login?)")
            )
        except Exception as e:
            self.print(f'failed to get file listing: {e}')
            return self.result.warn(
                feedback=f"Failed to retrieve data from host {self.host} as user {self.config.username}",
                staff_details=self.get_staff_error(e, "failed to get file listing (delayed failed login?)")
            )

        try:
            # perform read/write check
            if self.args.check_write:
                return self.write_file_check(ftp)
            else:
                return self.read_file_check(ftp)
        finally:
            ftp.close()


if __name__ == "__main__":
    FTPCheck().execute()
