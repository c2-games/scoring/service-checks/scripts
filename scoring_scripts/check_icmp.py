#!/usr/bin/env python3
import ping3
from cyboard_check import C2GamesCheck

ping3.EXCEPTIONS = True


class ICMPCheck(C2GamesCheck):
    def execute(self):
        """Execute the Check."""
        details = {
            'target': self.host
        }
        try:
            self.print('starting ping to ', self.host)
            ping3.ping(self.host, timeout=self.args.timeout)
        except ping3.errors.Timeout as e:
            details['raw'] = str(e)
            self.result.fail(
                feedback=f'Request Timed Out to host {self.host} after {self.args.timeout} seconds',
                staff_details=details
            )
        except ping3.errors.HostUnknown as e:
            details['raw'] = str(e)
            self.result.error(
                feedback=f'Could not resolve host: {self.host}',
                staff_details=details
            )
        except ping3.errors.DestinationHostUnreachable as e:
            details['raw'] = str(e)
            self.result.fail(
                feedback=f'ping says "Destination Host Unreachable" to host {self.host}',
                staff_details=details
            )
        except ping3.errors.DestinationUnreachable as e:
            details['raw'] = str(e)
            self.result.error(
                feedback=f'ping says "Destination Unreachable" to host {self.host}',
                staff_details=details
            )
        except ping3.errors.PingError as e:
            details['raw'] = str(e)
            self.result.error(
                feedback=f"An unknown ping error occurred to host {self.host}",
                staff_details=details
            )

        self.result.success(feedback=f'ping successful to host {self.host}')


if __name__ == '__main__':
    ICMPCheck().execute()
