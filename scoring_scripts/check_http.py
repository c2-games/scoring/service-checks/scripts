#!/usr/bin/env python3
import argparse
import requests

from cyboard_check import C2GamesCheck


class BasicHTTPCheck(C2GamesCheck):
    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Connect to web and check if HTTP content can be retrieved.'
        )
        # try to get /robots.txt even if it doesn't exist, because PHP can take a long time to respond
        parser.add_argument('-p', '--path', default='/robots.txt', help='URL on the server to retrieve')
        super().__init__(parser=parser)

    def execute(self) -> None:
        """Execute the Check."""
        response = None
        url = f"http://{self.host}{self.args.path}"
        details: dict = {
            'timeout': self.args.timeout
        }
        staff_details = {
            'url': url
        }
        try:
            response = requests.get(url, timeout=self.args.timeout)
            staff_details['status_code'] = str(response.status_code)
        except requests.exceptions.ConnectionError as e:
            self.result.fail(
                feedback="Failed to connect to server, is port 80 open?",
                details=details,
                staff_details={
                    'raw': str(e)
                }
            )
        except Exception as e:
            details['raw'] = str(e)
            self.result.fail(feedback="HTTP not found", details=details)
        finally:
            if response:
                response.close()
        self.result.success(feedback="HTTP Accessible", details=details, staff_details=staff_details)


if __name__ == '__main__':
    BasicHTTPCheck().execute()
