FROM registry.gitlab.com/c2-games/scoring/service-checks/check-daemon:latest
LABEL maintainer="c2games"

RUN apk -U upgrade --no-cache && \
    apk add --no-cache --update \
    ca-certificates \
    curl \
    jq \
    bash \
    python3 \
    shadow \
    iputils \
    tzdata \
    openssh \
    grep \
    sed \
    py3-pip \
    py3-dnspython \
    wget \
    lftp \
    mysql-client \
    bind-tools

COPY requirements.txt /opt/scripts/

RUN python3 -m pip install --break-system-packages -U pip && \
    python3 -m pip install -r /opt/scripts/requirements.txt --break-system-packages

COPY scoring_scripts /opt/scripts/

RUN groupmod -g 1000 users && \
    useradd -u 911 -U -d /opt/scripts -s /bin/false abc && \
    usermod -G users abc && \
    chown -R abc:abc /opt/scripts && \
    chmod a+x /opt/scripts/*.py

ENTRYPOINT ["/usr/local/bin/check-daemon"]
CMD []
